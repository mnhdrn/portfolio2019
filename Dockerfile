#==============================================================================#
#------------------------------------------------------------------------------#
# BUILD IMAGES

FROM elixir:slim as builder

ARG secret

RUN apt update -y && apt install -y git inotify-tools curl wget nodejs npm python make g++ 

#RUN apk --update add git inotify-tools curl wget nodejs npm python make g++ \
#&& rm -rf /var/cache/apk/*

RUN mkdir -p /build
COPY . /build

WORKDIR /build
RUN rm -rf ./_build
RUN rm -rf ./deps

ENV SECRET_KEY_BASE="$secret"

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix archive.install hex phx_new --force

RUN mix do deps.get, deps.compile, release.init

RUN npm install --prefix ./assets
RUN npm rebuild --prefix ./assets
RUN npm run deploy --prefix ./assets

RUN mix compile

RUN mix phx.digest
RUN MIX_ENV=releases mix compile
RUN MIX_ENV=releases mix release --overwrite

#==============================================================================#
#------------------------------------------------------------------------------#
# RELEASE IMAGES

FROM elixir:slim as app

RUN apt -y update && apt install -y openssl

RUN mkdir -p /app && mkdir -p priv && mkdir -p priv/repo
WORKDIR /app

COPY --from=builder /build/_build/releases/rel/portfolio ./
COPY --from=builder /build/priv/data.csv ./priv/data.csv

CMD exec bin/portfolio start
