defmodule Portfolio.Email do
  import Bamboo.Email

  def contact_email(email, message) do
    new_email(
      to: "contact.mnhdrn@gmail.com",
      from: "contact@mnhdrn.com",
      subject: "new message from: " <> email,
      html_body: "<strong>" <> message <> "</strong>",
      text_body: message
    )
  end

end
