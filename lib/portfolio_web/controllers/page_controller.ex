defmodule PortfolioWeb.PageController do
  use PortfolioWeb, :controller

  alias Portfolio.Server

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def show(conn, %{"slug" => slug}) do
    article = Server.get(slug)

    if article == [] do
      redirect(conn, to: "/error/404")
    else
      render(conn, "show.html")
    end
  end
end
