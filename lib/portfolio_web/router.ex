defmodule PortfolioWeb.Router do
  use PortfolioWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :put_secure_browser_headers
  end

  scope "/", PortfolioWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/:slug", PageController, :show
  end

    scope "/api", PortfolioWeb do

      post "/send_mail", ApiController, :contact_email
      get "/get/:title", ApiController, :get_articles
      get "/get_list", ApiController, :get_list
      get "/get_tag", ApiController, :get_tag
    end
end
