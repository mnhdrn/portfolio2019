// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"
//
import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import i18n from './i18n.js';
import Cookies from 'js-cookie';

import Nav from "./components/nav.js"
import Content from "./components/content.js"
import Paralax from "./components/paralax.js"
import Rgpd from "./components/rgpd.js"

//----------------------------------- Lang cookie
//-----------------------------------

if (Cookies.get('lng') !== undefined)
	i18n.changeLanguage(Cookies.get('lng'));

//----------------------------------- api call to unsplash
//-----------------------------------
//
const Http = new XMLHttpRequest();
const url='https://source.unsplash.com/user/mnhdrn/likes';
Http.open("GET", url);
Http.send();

Http.onreadystatechange = (e) => {
	if (Http.readyState === 4) {
		if (Http.status !== 200) {
			document.getElementById('paralax').classList.add('default');
			document.querySelector('nav').classList.add('default');
		}
	}
}

//----------------------------------- app component def
//-----------------------------------

class App extends Component {
	constructor(props) {
		super(props)

		this.handleRgpd = this.handleRgpd.bind(this)
	}

	handleRgpd() {
		const el = document.getElementById('nag');

		if (Cookies.get('lng') === undefined)
			Cookies.set('lng', i18n.language);
		i18n.changeLanguage(Cookies.get('lng'));

		el.remove()
	}

	render () {
		return (
			<div>
				{(Cookies.get('lng') === undefined) ? <Rgpd action={this.handleRgpd} /> : null}
				<Nav />
				<Content />
				<Paralax />
			</div>
		);
	}
};

ReactDOM.render( <App /> , document.getElementById("app"));
