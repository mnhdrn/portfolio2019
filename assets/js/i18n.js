import i18n from 'i18next';
import { initReactI18next, reactI18nextModule } from "react-i18next";
import LanguageDetector from 'i18next-browser-languagedetector';


import jsonEN from '../static/i18n/en.json';
import jsonFR from '../static/i18n/fr.json';

i18n
	.use(initReactI18next)
	.use(LanguageDetector)
	.init({
		// we init with resources
		resources: {
			en: {
				translations: jsonEN
			},
			fr: {
				translations: jsonFR
			}
		},
		lng: "fr",
		fallbackLng: 'fr',
		debug: false,

		// have a common namespace used around the full app
		ns: ['translations'],
		defaultNS: 'translations',


		interpolation: {
			escapeValue: false, // not needed for react!!
			formatSeparator: ','
		},

		react: {
			wait: false,
			bindI18n: 'languageChanged loaded',
			bindStore: 'added removed',
			nsMode: 'translations'
		}
	});

export default i18n;
