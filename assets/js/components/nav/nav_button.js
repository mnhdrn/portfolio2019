import React, {Component} from 'react';

class NavButton extends Component {
	constructor () {
		super();

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick = () => {
		let buttonEl = ' ' + nav_but.className + ' '
		let bgEl = ' ' + paralax.className + ' '
		let overlayEl = ' ' + nav_overlay.className + ' '

		if ( ~buttonEl.indexOf(' is-active ') ) {
			nav_but.className = buttonEl.replace(' is-active ', '');
		} else {
			nav_but.className += ' is-active ';
		};

		if ( ~bgEl.indexOf(' is-blur ') ) {
			paralax.className = bgEl.replace(' is-blur ', '');
		} else {
			paralax.className += ' is-blur';
		};

		if ( ~overlayEl.indexOf(' is-active ') ) {
			nav_overlay.className = overlayEl.replace(' is-active ', '');
		} else {
			nav_overlay.className += ' is-active ';
		};

	};

	render () {
		return (
			<button
			id="nav_but" className="hamburger hamburger--squeeze no-border" type="button" onClick={this.handleClick}>
			<span className="hamburger-box">
				<span className="hamburger-inner"></span>
			</span>
			</button>
		);
	};
}

export default NavButton
