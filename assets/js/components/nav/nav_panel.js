import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';
import Cookies from 'js-cookie'


class NavPanel extends Component {
	constructor (props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick = () => {
		let buttonEl = ' ' + nav_but.className + ' '
		let bgEl = ' ' + paralax.className + ' '
		let overlayEl = ' ' + nav_overlay.className + ' '

		if ( ~buttonEl.indexOf(' is-active ') ) {
			nav_but.className = buttonEl.replace(' is-active ', '');
		} else {
			nav_but.className += ' is-active ';
		};

		if ( ~bgEl.indexOf(' is-blur ') ) {
			paralax.className = bgEl.replace(' is-blur ', '');
		} else {
			paralax.className += ' is-blur';
		};

		if ( ~overlayEl.indexOf(' is-active ') ) {
			nav_overlay.className = overlayEl.replace(' is-active ', '');
		} else {
			nav_overlay.className += ' is-active ';
		};

	};

	render () {

		const { t, i18n } = this.props;

		const changeLanguage = (lng) => {
			i18n.changeLanguage(lng);
			this.handleClick();
			Cookies.set('lng', i18n.language);
		}

		return (
			<div id="nav_panel">
			<ul>
				<li> <a href="/#" title={t('home')} alt={t('home')} onClick={this.handleClick}> {t('home')} </a> </li>
				<li> <a href="/#articles_section" title={t('articles')} alt={t('articles')} onClick={this.handleClick}> {t('articles')} </a> </li>
				<li> <a href="/#separator_section" title={t('about')} alt={t('about')} onClick={this.handleClick}> {t('about')} </a> </li>
				<li> <a href="/#contact_section" title={t('contact')} alt={t('contact')} onClick={this.handleClick}> {t('contact')} </a> </li>
			</ul>
			<div id="langage_select">
				<span onClick={ () => changeLanguage('fr') }>fr</span>
				|
				<span onClick={ () => changeLanguage('en') }>en</span>
			</div>
			</div>
		);
	};
}

export default withTranslation()(NavPanel);
