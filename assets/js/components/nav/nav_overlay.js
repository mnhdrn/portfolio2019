import React, {Component} from 'react';


class NavOverlay extends Component {
	constructor (props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick = () => {
		let buttonEl = ' ' + nav_but.className + ' '
		let bgEl = ' ' + paralax.className + ' '
		let overlayEl = ' ' + nav_overlay.className + ' '

		if ( ~buttonEl.indexOf(' is-active ') ) {
			nav_but.className = buttonEl.replace(' is-active ', '');
		} else {
			nav_but.className += ' is-active';
		};

		if ( ~bgEl.indexOf(' is-blur ') ) {
			paralax.className = bgEl.replace(' is-blur ', '');
		} else {
			paralax.className += ' is-blur';
		};

		if ( ~overlayEl.indexOf(' is-active ') ) {
			nav_overlay.className = overlayEl.replace(' is-active ', '');
		} else {
			nav_overlay.className += ' is-active ';
		};
	}

	render () {
		return (
			<div id="nav_overlay" onClick={this.handleClick}> </div>
		);
	};
}

export default NavOverlay;
