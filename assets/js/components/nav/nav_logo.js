import React, {Component} from 'react';

class NavLogo extends Component {
	render () {
		return (
			<div id="nav_logo">
				<a href="/" title="Go to the top" alt="top logo"></a>
			</div>
		);
	};
}

export default NavLogo
