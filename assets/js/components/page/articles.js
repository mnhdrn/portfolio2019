import React, {Component} from 'react';
import Axios from "axios";
import Cookies from "js-cookie";
import { withTranslation } from 'react-i18next';

import Header from "./articles/header.js"
import ArticleContent from "./articles/content.js"
import Footer from "./footer.js"

class Articles extends Component {

	constructor(props) {
		super(props);
		this.state = {
			data: {}
		}
	}

	componentWillMount() {
		Axios.get('/api/get' + window.location.pathname)
			.then((response) => {
				return response.data;
			})
			.then ((data) => {
				this.setState({
					data: data.data
				});
			})
			.catch (
				error => console.log(error)
			);
	}

	render() {

		const { t, i18n } = this.props;

		const title = (i18n.language == "fr") ? this.state.data.title_fr : this.state.data.title_en
		const text = (i18n.language == "fr") ? this.state.data.txt_fr : this.state.data.txt_en

		return (
			<div id="page_articles_container">
				<Header title={title} />
				<ArticleContent title={title} text={text} />
				<Footer />
			</div>
		);
	}
}

export default withTranslation()(Articles);
