import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';

class Footer extends Component {
	constructor (props) {
		super(props);

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick = () => {
		let buttonEl = ' ' + nav_but.className + ' '
		let bgEl = ' ' + paralax.className + ' '

		if ( ~buttonEl.indexOf(' is-active ') ) {
			nav_but.className = buttonEl.replace(' is-active ', '');
		};

		if ( ~bgEl.indexOf(' is-blur ') ) {
			paralax.className = bgEl.replace(' is-blur ', '');
		};

	};

	render () {

		const { t, i18n } = this.props;

		const changeLanguage = (lng) => {
			i18n.changeLanguage(lng);
		}

		let year = new Date().getFullYear();

		return (
			<footer>
			<div id="left">
				<ul>
					<li> <a href="/#" title={t('home')} alt={t('home')} onClick={this.handleClick}> {t('home')} </a> </li>
					<li> <a href="/#articles_section" title={t('articles')} alt={t('articles')} onClick={this.handleClick}> {t('articles')} </a> </li>
					<li> <a href="/#separator_section" title={t('about')} alt={t('about')} onClick={this.handleClick}> {t('about')} </a> </li>
					<li> <a href="/#contact_section" title={t('contact')} alt={t('contact')} onClick={this.handleClick}> {t('contact')} </a> </li>
				</ul>
			</div>
			<div id="right">
				<ul>

					<li><a href="https://www.linkedin.com/in/cl%C3%A9ment-richard-186201142/" title={t('linkedin_button')} alt={t('linkedin_button')} target="_blank"> Linkedin</a> </li>
					<li><a href="https://gitlab.com/mnhdrn" title={t('gitlab_button')} alt={t('gitlab_button')} target="_blank"> Gitlab</a> </li>
				</ul>
			</div>
			<div id="bottom">
			<p>{t('footer_right')} {year} {t("footer_left")}<a href=""> MNHDRN</a></p>
			</div>
			</footer>
		);
	}
};

export default withTranslation()(Footer)
