import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';
import { faArrowLeft } from '@fortawesome/fontawesome-free-solid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Header extends Component {
	render () {

		const { t, i18n } = this.props;

		//			<button>{t('begin')}</button>
		return (
		<header id="articles_header">
			<h1>{this.props.title}</h1>
			<a href="/" alt="">
				<button>
					<FontAwesomeIcon className="icon-FontAwesome" icon={faArrowLeft} />
					{t('back')}
				</button>
			</a>
		</header>
		);
	}
};

export default withTranslation()(Header);
