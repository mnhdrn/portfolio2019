import React, {Component} from 'react';
import {markdown} from 'markdown';
import { withTranslation } from 'react-i18next';
import { faArrowLeft } from '@fortawesome/fontawesome-free-solid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class ArticleContent extends Component {
	constructor(props) {
		super(props);

		this.handleMarkdown = this.handleMarkdown.bind(this);
	}

	handleMarkdown(text) {
		return markdown.toHTML(text);
	}

	render () {
		const { t, i18n } = this.props;

		let text = (this.props.text) ? this.handleMarkdown(this.props.text
			.replace(/\\1/g, "  \n")
			.replace(/\\2/g, "\n\n")
			.replace(/\s+#/g, "\n\n#")) : t('articles_error');

//		<h2>{this.props.title}</h2>
		return (
			<section id="content_section" className="top_section">

			<div className="markdown" dangerouslySetInnerHTML={{__html: text}}></div>

				<div id="share_container">
					<a href="/" title={t('back')} alt="Go back button">
						<FontAwesomeIcon className="icon-FontAwesome" icon={faArrowLeft} />
						{t('back')}
					</a>
					<a href="https://gitlab.com/mnhdrn" title={t('gitlab_button')} alt={t('gitlab_button')} target="_blank">
						<img src="https://png.pngtree.com/svg/20161103/c7ded8218b.svg" alt="gitlab.com logo"/>
						{t('gitlab_button')}
					</a>
					<a href="https://www.linkedin.com/in/cl%C3%A9ment-richard-186201142/" title={t('linkedin_button')} alt={t('linkedin_button')} target="_blank">
						<img src="https://image.flaticon.com/icons/png/512/61/61109.png" alt="linkedin.com logo"/>
						{t('linkedin_button')}
					</a>
				</div>

			</section>
		);
	}
};

export default withTranslation()(ArticleContent);
