import React, {Component} from 'react';
import {markdown} from 'markdown';
import { withTranslation } from 'react-i18next';

import TechContainer from "./about/tech_container.js";

class About extends Component {
	constructor(props) {
		super(props);

		this.handleMarkdown = this.handleMarkdown.bind(this);
	}

	handleMarkdown(text) {
		return markdown.toHTML(text);
	}

	render () {

		const { t, i18n } = this.props;
		const text = this.handleMarkdown(t('about_text'));

		return (
			<section id="about_section" className="top_section">
			<div className="profile">
				<img src="/images/mnhdrn.jpg" title="Clément Richard" alt="Clément Richard"/>
			</div>
			<div className="markdown_container"dangerouslySetInnerHTML={{__html: text}}></div>
				<div id="share_container">
					<a href="https://gitlab.com/mnhdrn" title={t('gitlab_button')} alt={t('gitlab_button')} target="_blank">
						<img src="https://png.pngtree.com/svg/20161103/c7ded8218b.svg" alt="gitlab.com logo"/>
						{t('gitlab_button')}
					</a>
					<a href="https://www.linkedin.com/in/cl%C3%A9ment-richard-186201142/" title={t('linkedin_button')} alt={t('linkedin_button')} target="_blank">
						<img src="https://image.flaticon.com/icons/png/512/61/61109.png" alt="linkedin.com logo"/>
						{t('linkedin_button')}
					</a>
					<a href="mailto:contact.mnhdrn@gmail.com" title="contact.mnhdrn@gmail.com" alt="contact.mnhdrn@gmail.com" target="_blank">
						<img src="https://image.flaticon.com/icons/svg/126/126412.svg" alt="mail logo"/>
						{t('mail_button')}
					</a>
				</div>

			<TechContainer />

			</section>
		);
	}
};
//<h1> {t('about_me')} </h1>

export default withTranslation()(About)
