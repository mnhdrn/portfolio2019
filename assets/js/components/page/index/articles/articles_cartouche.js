import React, {Component} from 'react';

class ArticlesCartouches extends Component {
	render() {

		const handleTitle = this.props.title.length > 16 ? this.props.title.substring(0, 18) + " ..." : this.props.title;

		const reftitle = "/" + this.props.reftitle.replace("?", "%3F");

		return (
			<figure className={this.props.classHide + "articles_cartouche"}>
			<a href={reftitle} title={this.props.title}>
			<div className="title">{handleTitle}</div>
			<div className="tag">{this.props.tag}</div>
			<div className="img">
			<img src={this.props.thumb_img} alt={this.props.title}/>
			</div>
			</a>
			</figure>
		);
	}
}

export default ArticlesCartouches;
