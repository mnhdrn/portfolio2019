import React, {Component} from 'react';
import Cookies from "js-cookie";
import { withTranslation } from 'react-i18next';

import ArticlesCartouche from './articles_cartouche.js';

class ArticlesContainer extends Component {

	render () {

		let arr_articles = [];

		const { t, i18n } = this.props;

		const searchWork = this.props.search_value == "" && this.props.tag_value == "";

		if (this.props.articles_list[0] !== undefined && searchWork == true)
			arr_articles = this.props.articles_list
		else if (this.props.articles_search.length != 0)
			arr_articles = this.props.articles_search;

		const seedArticles = arr_articles.map((current, index) => {
			return (
				 <ArticlesCartouche
					title={(i18n.language == "fr") ? current.title_fr : current.title_en}
					reftitle={current.title_en}
					id={current.id}
					tag={current.tag}
					thumb_img={current.thumb}
					key={current.title_en}
					classHide={index >= this.props.articles_nb && searchWork == true ? "hidden " : ""}
				/>
			)
		});

		return (
				<div className="articles_container">
					{seedArticles}
				</div>
		);
	}
};

export default withTranslation()(ArticlesContainer);
