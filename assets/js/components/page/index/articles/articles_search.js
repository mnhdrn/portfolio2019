import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/fontawesome-free-solid'
import { withTranslation } from 'react-i18next';

class ArticlesSearch extends Component {
	constructor(props) {
		super(props)

		this.handleClickInput = this.handleClickInput.bind(this)
	}

	handleClickInput () {
		search_bar.focus();
	}

	render() {

		const { t, i18n } = this.props;

		return (
			<div id="articles_search">
				<div className="search">

					<label htmlFor="search" className="inp">
						<input id="search_bar" type="text" name="search" placeholder=" "
						value={this.props.search_value} onChange={(e) => {this.props.action_onchange(e)}} />
						<span className="label" onClick={this.handleClickInput}>{t('search')}</span>
						<span className="border"></span>
					</label>
				</div>
			</div>
		)
	};
}

export default withTranslation()(ArticlesSearch);
