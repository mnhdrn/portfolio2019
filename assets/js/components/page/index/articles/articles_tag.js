import React, {Component} from 'react';
import Axios from "axios";

class ArticlesTag extends Component {
	constructor(props) {
		super(props);

		this.state = {
			tagSelect: '',
			tagList: []
		};

		this.handleClick = this.handleClick.bind(this);
	};

	componentWillMount() {
		Axios.get('/api/get_tag')
			.then((response) => {
				return response.data;
			})
			.then ((data) => {
				this.setState({
					tagList: data.data
				});
			})
			.catch (error => console.log(error));
	}

	handleClick = (element) => {

		if (~element.target.className.indexOf(' active ')) {
			element.target.className = element.target.className.replace(' active ', '');
			this.props.action("");
		} else {
			const allTag = document.querySelectorAll(".tagItem");

			allTag.forEach((e) => {
				if ( ~e.className.indexOf(' active ') ) {
					e.className = e.className.replace(' active ', '');
				}
			});

			element.target.className += " active ";
			this.props.action(element.target.innerHTML.toString());
		};
	};

	render() {

		const seedTag = this.state.tagList.map((tagName, index) => {
			return (
				<div className="tagItem" onClick={this.handleClick} key={tagName + "ywh"}>{tagName}</div>
			);
		})

		return (
			<div id="articles_tag">
				{seedTag}
			</div>
		)
	}
}

export default ArticlesTag;
