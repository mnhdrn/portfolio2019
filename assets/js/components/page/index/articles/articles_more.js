import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { withTranslation } from 'react-i18next';
import ArticlesCartouche from "./articles_cartouche.js";

class ArticlesMore extends Component {
	render() {

		const { t, i18n } = this.props;

		const searchWork = this.props.search_value == "" && this.props.tag_value == "";

		const hiddenButton = this.props.articles_nb >= this.props.articles_max || searchWork == false ? "hidden" : "";

		return (
			<div id="articles_more" className={hiddenButton} >
				<button className="loadMoreButton" onClick={this.props.action}>{t('load_more')}</button>
			</div>
		)
	}
}

export default withTranslation()(ArticlesMore);
