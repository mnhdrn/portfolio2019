import React, {Component} from 'react';
import { withTranslation } from 'react-i18next';
import { faTimes } from '@fortawesome/fontawesome-free-solid'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Rgpd extends Component {
	constructor(props) {
		super(props)

		this.handleClose = this.handleClose.bind(this)
	}

	handleClose() {
		const el = document.getElementById('nag');
		el.remove()
	}

	render () {

		const { t, i18n } = this.props;

		return (
			<div id="nag">
				<div>
						<span>{t('rgpd')}</span>
						<button onClick={this.props.action}>
							{t('accept')}
						</button>
					 <FontAwesomeIcon className="icon-FontAwesome" icon={faTimes} onClick={this.handleClose} />
				</div>
			</div>
		);
	}
};

export default withTranslation()(Rgpd);
