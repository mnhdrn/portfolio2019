import React, {Component} from 'react';

import NavButton from './nav/nav_button.js';
import NavPanel from './nav/nav_panel.js';
import NavOverlay from './nav/nav_overlay.js';
import NavLogo from './nav/nav_logo.js';

class Nav extends Component {
	constructor(props) {
		super(props);

		this.listenToScroll = this.listenToScroll.bind(this);
	}

	componentDidMount() {
		window.addEventListener('scroll', this.listenToScroll)
		window.addEventListener('resize', this.listenToScroll)
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.listenToScroll)
		window.removeEventListener('resize', this.listenToScroll)
	}

	listenToScroll = () => {
		const articles_page = !! document.getElementById("page_articles")

		const winScroll = document.body.scrollTop || document.documentElement.scrollTop

		let winHeight = document.documentElement.clientHeight

		if (articles_page) {
			winHeight = winHeight / 2 - 100
		}


		if (winScroll > (winHeight - 50))
			document.querySelector('nav').classList.add("navbar--active");
		else
			document.querySelector('nav').classList.remove("navbar--active");
	}

	render () {
		return (
			<nav>
				<NavButton />
				<NavPanel />
				<NavLogo />
				<NavOverlay />
			</nav>
		);
	}
};

export default Nav
