import React, {Component} from 'react';

import Index from './page/index.js'
import Articles from './page/articles.js'
import Error from './page/error.js'


class Content extends Component {
	render () {

		const indexQuery = !!document.getElementById("page_index");
		const articlesQuery = !!document.getElementById("page_articles");
		const errorQuery = !!document.getElementById("page_error");

		return (
			<div>
			{ indexQuery == true ?  <Index /> : null}
			{ articlesQuery == true ?  <Articles /> : null}
			{ indexQuery == false && articlesQuery == false ? <Error /> : null}
			</div>
		);
	}
};

export default Content
